package net.selleukidd.servercore.listener;

import net.selleukidd.servercore.ServerCore;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static net.selleukidd.servercore.ServerCore.api;

/**
 * Created by marcel on 21.05.17.
 */
public class PlayerJoin implements Listener {
    @EventHandler
    public void PlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        e.setJoinMessage("");
        Bukkit.getScheduler().runTaskLater(ServerCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                p.setPlayerListName(ChatColor.GRAY + p.getName());
                p.setPlayerListName(ChatColor.GRAY + p.getName());
                p.setPlayerListName(ChatColor.GRAY + p.getName());
                if (api.getGroup(api.getUser(e.getPlayer().getDisplayName()).getPrimaryGroup()).hasPermission(api.getNodeFactory().makeMetaNode("namecolor", "darkred").build()).asBoolean())
                    p.setPlayerListName(ChatColor.DARK_RED + p.getName());
                if (api.getGroup(api.getUser(e.getPlayer().getDisplayName()).getPrimaryGroup()).hasPermission(api.getNodeFactory().makeMetaNode("namecolor", "darkgreen").build()).asBoolean())
                    p.setPlayerListName(ChatColor.DARK_GREEN + p.getName());
                if (api.getGroup(api.getUser(e.getPlayer().getDisplayName()).getPrimaryGroup()).hasPermission(api.getNodeFactory().makeMetaNode("namecolor", "darkred").build()).asBoolean())
                    p.setPlayerListName(ChatColor.DARK_RED + p.getName());
                if (api.getGroup(api.getUser(e.getPlayer().getDisplayName()).getPrimaryGroup()).hasPermission(api.getNodeFactory().makeMetaNode("namecolor", "darkgreen").build()).asBoolean())
                    p.setPlayerListName(ChatColor.DARK_GREEN + p.getName());
                if (api.getGroup(api.getUser(e.getPlayer().getDisplayName()).getPrimaryGroup()).hasPermission(api.getNodeFactory().makeMetaNode("namecolor", "darkred").build()).asBoolean())
                    p.setPlayerListName(ChatColor.DARK_RED + p.getName());
                if (api.getGroup(api.getUser(e.getPlayer().getDisplayName()).getPrimaryGroup()).hasPermission(api.getNodeFactory().makeMetaNode("namecolor", "darkgreen").build()).asBoolean())
                    p.setPlayerListName(ChatColor.DARK_GREEN + p.getName());
            }
        }, 10L);
    }
}
