package net.selleukidd.servercore.listener;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.SpawnEgg;

/**
 * Created by Marcel on 26.05.2017.
 */
public class ServerCompass implements Listener {
    @EventHandler
    public void onOpen(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) | e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (e.getPlayer().getItemInHand().getType().equals(Material.COMPASS)) {
                Inventory inv = Bukkit.createInventory(null, 9, "» Serverauswahl");

                // Spawn
                ItemStack i1 = new ItemStack(Material.EYE_OF_ENDER);
                ItemMeta im = i1.getItemMeta();
                im.setDisplayName(ChatColor.DARK_GREEN + "Spawn");
                i1.setItemMeta(im);

                // TubeTown
                ItemStack i2 = new ItemStack(Material.NOTE_BLOCK);
                im = i2.getItemMeta();
                im.setDisplayName("TubeTown");
                i2.setItemMeta(im);

                // Epona

                ItemStack i3 = new ItemStack(Material.SADDLE);
                im = i3.getItemMeta();
                im.setDisplayName("Epona");
                i3.setItemMeta(im);

                // Exo

                ItemStack i4 = new ItemStack(Material.REDSTONE);
                im = i4.getItemMeta();
                im.setDisplayName("Exo");
                i4.setItemMeta(im);

                // Exfy

                ItemStack i5 = new SpawnEgg(EntityType.BLAZE).toItemStack();
                im = i5.getItemMeta();
                im.setDisplayName("Exfy");
                i5.setItemMeta(im);

                // Exit

                ItemStack i6 = new ItemStack(Material.FIREBALL);
                im = i6.getItemMeta();
                im.setDisplayName("Schließen");
                i6.setItemMeta(im);

                inv.setItem(0, i1);
                inv.setItem(1, i2);
                inv.setItem(2, i3);
                inv.setItem(3, i4);
                inv.setItem(4, i5);
                inv.setItem(8, i6);
                e.getPlayer().openInventory(inv);
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory().getName().equals("» Serverauswahl")) {
            if (e.isLeftClick()) {
                switch (e.getCurrentItem().getType()) {
                    case EYE_OF_ENDER:
                        p.teleport(Bukkit.getWorld("world").getSpawnLocation());
                        p.sendMessage("Hallo");
                        p.closeInventory();
                        break;
                    case NOTE_BLOCK:
                        ByteArrayDataOutput out = ByteStreams.newDataOutput();
                        out.writeUTF("Connect");
                        out.writeUTF("tubetown");
                        p.sendPluginMessage(Bukkit.getPluginManager().getPlugin("ServerCore"), "BungeeCord", out.toByteArray());
                        p.closeInventory();
                        break;
                    case SADDLE:
                        p.closeInventory();
                        break;
                    case REDSTONE:
                        p.closeInventory();
                        break;
                    case MONSTER_EGG:
                        p.closeInventory();
                        break;
                    case FIREBALL:
                        p.closeInventory();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
