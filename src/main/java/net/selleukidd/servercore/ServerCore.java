package net.selleukidd.servercore;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.LuckPermsApi;
import net.selleukidd.servercore.commands.cmd_fly;
import net.selleukidd.servercore.commands.cmd_nope;
import net.selleukidd.servercore.listener.PlayerJoin;
import net.selleukidd.servercore.listener.ServerCompass;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by marcel on 14.05.17.
 */
public class ServerCore extends JavaPlugin {

    public static LuckPermsApi api;
    private static Connection conn;
    private static ServerCore instance;

    @Override
    public void onEnable() {
        instance = this;
        api = LuckPerms.getApi();
        loadMySQL();
        loadCommands();
        loadListeners();
    }

    private void loadMySQL() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/servercore?"
                    + "user=servercore&password=tYCvWvCu780ROAkG");
            getLogger().config("MySql Connection successfully!");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void loadCommands() {
        getCommand("plugin").setExecutor(new cmd_nope());
        getCommand("pl").setExecutor(new cmd_nope());
        getCommand("?").setExecutor(new cmd_nope());
        getCommand("help").setExecutor(new cmd_nope());
        getCommand("me").setExecutor(new cmd_nope());
        getCommand("fly").setExecutor(new cmd_fly());
    }

    private void loadListeners() {
        getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
        getServer().getPluginManager().registerEvents(new ServerCompass(), this);
    }

    public static Connection getMySql() {
        return conn;
    }

    public static JavaPlugin getInstance() {
        return instance;
    }
}
