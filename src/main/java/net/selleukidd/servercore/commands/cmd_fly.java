package net.selleukidd.servercore.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by marcel on 15.05.17.
 */
public class cmd_fly implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player)sender;
        if(!p.getAllowFlight()) {
            p.setAllowFlight(true);
            p.sendMessage("Flying enabled");
        } else {
            p.setAllowFlight(false);
            p.sendMessage("Flying disabled");
        }
        return false;
    }
}
